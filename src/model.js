import image from './assets/wide-angle-lens.jpg';
import {ImageBlock, TextBlock, TextColumnsBlock, TitleBlock} from './classes/blocks';
import {css} from './utils';

export const model = [
	new TitleBlock(
		{
			styles: css({
				'background': 'lightblue',
				'color': '#000',
				'font-weight': '700',
				'text-align': 'center',
				'padding': '20px',
				'margin-bottom': '10px',
			}),
			tag: `h2`,
		}),

	new ImageBlock(
		image,
		{
			styles: css({
				'padding': '2rem 0',
				'display': 'flex',
				'justify-content': 'center',
			}),
			alt: 'my image',
			imageStyles: 'width: 500px; height: auto',
		}),

	new TextBlock(
		`Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad aliquam assumenda deleniti esse 
				expedita fuga itaque non quidem ullam?`,
		{
			styles: css({
				'background': '#000',
				'color': '#fff',
				'padding': '20px',
				'margin-bottom': '10px',
			}),
		}),

	new TextColumnsBlock(
		[
			`Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad aliquam assumenda deleniti esse
				expedita fuga itaque non quidem ullam?`, `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad aliquam assumenda deleniti esse
				expedita fuga itaque non quidem ullam?`, `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad aliquam assumenda deleniti esse
				expedita fuga itaque non quidem ullam?`,
		],
		{
			styles: css({
				'padding': '1rem',
				'background': 'lightgreen',
				'text-align': 'center',
				'margin-bottom': '10px',
			}),
		}),

	new TextBlock(
		`Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad aliquam assumenda deleniti esse\n' +
				expedita fuga itaque non quidem ullam? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad aliquam assumenda deleniti esse\n' +
				expedita fuga itaque non quidem ullam? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad aliquam assumenda deleniti esse\n' +
				expedita fuga itaque non quidem ullam? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ad aliquam assumenda deleniti esse\n' +
				expedita fuga itaque non quidem ullam?`,
		{
			styles: css({
				'background': 'lightgreen',
				'text-align': 'center',
				'color': '#fff',
				'padding': '20px',
				'margin-bottom': '10px',
			}),
		}),
];